<?php

function requestTicket(){
    $wsdl = "https://paycenter.piraeusbank.gr/services/tickets/issuer.asmx?WSDL";

    $soapParams = new stdClass();
    $soapParams->Request->Username = "KT235056";
    $soapParams->Request->Password = md5("LS976385");
    $soapParams->Request->MerchantId = 2133499495;
    $soapParams->Request->PosId = 2131019924;
    $soapParams->Request->AcquirerId = 14;
    $soapParams->Request->MerchantReference = 10001;
    $soapParams->Request->RequestType = '02';  //02 = Sale
    $soapParams->Request->ExpirePreauth = 0;
    $soapParams->Request->Amount = 10;
    $soapParams->Request->CurrencyCode = 978; // Euro = 978
    $soapParams->Request->Installments = 0;
    $soapParams->Request->Bnpl = 0;
    $soapParams->Request->Parameters = '';

    $params = getDefaultSoapParams();
    try {
        $client = new SoapClient($wsdl, $params);
        $result = $client->__soapCall("IssueNewTicket",array($soapParams));
    } catch (SoapFault $fault ){
        echo($fault->faultstring);
        //$lr = $client->__getLastRequest();
        // JLog::add('__soapCall Exeption: ' . $fault->faultstring, JLog::WARNING,'com_imeticket');
        // JLog::add('__soapCall Exeption: ' . $fault->faultstring, JLog::WARNING,'jerror');
        // JFactory::getApplication()->enqueueMessage($fault->faultstring,'error');
    }

    if (is_soap_fault($result)) {
        echo($result->faultstring);
        //trigger_error("SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faultstring})", E_USER_ERROR);
        // JLog::add('__soapCall Exeption: ' . $fault->faultstring, JLog::WARNING,'com_imeticket');
        // JLog::add('__soapCall Exeption: ' . $fault->faultstring, JLog::WARNING,'jerror');
    }

    return (isset($result->IssueNewTicketResult)) ? $result->IssueNewTicketResult: null;

}

function getDefaultSoapParams(){
    $opts = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );

    // SOAP 1.2 client
    return array(
        'encoding' => 'UTF-8',
        'verifypeer' => false,
        'verifyhost' => false,
        'trace' => 1,
        'stream_context' => stream_context_create($opts)
    );

}


var_dump(requestTicket());
 

?>