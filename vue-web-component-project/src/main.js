// import Vue from 'vue'


// ------------------> vue-custom-element example

// import VueCustomElement from 'vue-custom-element'
// Vue.use(VueCustomElement)
// import HelloWorld from './components/HelloWorld.vue'
// Vue.customElement('hello-world', HelloWorld)

// ------------------> vue wrapper example

// import wrap from '@vue/web-component-wrapper';
// Vue.config.productionTip = false

// import Payment from './components/Payment.vue'
// const PaymentElement = wrap(Vue, Payment);

// window.customElements.define('payment-element', PaymentElement);


import Vue from 'vue';

/**
 * Uncomment if you want to play with web component
 */
//import wrap from './lib/component-wrapper';
import wrap from '@vue/web-component-wrapper'
import Payment from './components/Payment'
const PaymentElement = wrap(Vue, Payment);
window.customElements.define('payment-element', PaymentElement);

// import vueCustomElement from 'vue-custom-element'
// import Payment from './components/Payment.vue'
// Vue.use(vueCustomElement)
// Vue.customElement('payment-element', Payment);

//Vue.config.productionTip = false

/**
  Uncomment this to work as a lib
 * 
 */
//new Vue()




/**
   Uncomment this to work as vue app
 * 
 */

// import App from './App.vue'

// new Vue({
//   render: h => h(App)
// }).$mount('#app')
