export const RESPONSES = {
    "_UNKNOWN_": { code: -1, message: "An uknown error occured"},
    "_SUCCESS_": { code: 0, message: "Payment completed" },
    "_ALREADY_PROCCESSED_": { code: 1, message: "Payment already proccesed" },
    "_CANCELED_": { code: 2, message: "Payment Canceled from user" },
    "_DECLINED_": { code: 3, message: "Transaction declined" },
    "_TICKET_NOT_FOUND_": { code: 4, message: "Ticket not found" },
    "_INVALID_CARD_INFO_" : { code: 5, message: "Invalid card info or expiration"},
    "_NO_ACTIVE_TRANSACTION_FOUND_": { code: 6, message: "Service not found active transaction with this id" },
    "_NO_TRANSACTION_FOUND_": { code: 7, message: "Service not found transaction with this id" },
    "_COMPONENT_BAD_CONFIGURATION_": {code: 100, message: "Component configuration missing"},
    "_SERVICE_ERROR_": { code: 101, message: "" }
}

function buildResponse(code, body) {
    return { ...code, ...body }
}

export function success(body) {
    return buildResponse(RESPONSES._SUCCESS_, body);
}
export function alreadyProcessed(body) {
    return buildResponse(RESPONSES._ALREADY_PROCCESSED_, body);
}
export function serviceError(body) {
    return buildResponse(RESPONSES._SERVICE_COMMUNICATION_ERROR_, body)
}

