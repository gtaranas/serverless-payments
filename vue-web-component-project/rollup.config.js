import vue from 'rollup-plugin-vue'
import buble from 'rollup-plugin-buble'
import nodeResolve from 'rollup-plugin-node-resolve'
import gzip from "rollup-plugin-gzip";
import uglify from 'rollup-plugin-uglify';
import replace from 'rollup-plugin-replace';
import css from 'rollup-plugin-css-only'

export default {
    entry: 'src/main.js',
    dest: 'dist/helloworld-component.js',
    plugins: [
        css(),
        replace({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        nodeResolve({
            module: true,
            jsnext: true,
            main: true
        }),
        vue({
            compileTemplate: true,
            css:false
        }),
        buble(),
        uglify(),
        gzip()
    ]
}