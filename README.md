# Payments Web Component And Services

## Service

> Bank responses (ResponseCode)

| code           | description                             |Action                                      |
| ---------------|-----------------------------------------|--------------------------------------------|
|00, 08, 10 ,16  |Approved or completed succesfully        |
|11              |Already processed                        |
|05, 12, 51      |Declined - Απορρίφθηκε απο την τραπεζα   |Contact with bank or try another card
|34, 43          |Lost card, Stolen card                   |Contact with bank or try another card
|54              |Expired card                             |Try another card
|62              |Restricted Card                          |Contact with bank or try another card
|92              |Declined                                 |Try again                                  |

> Bank errors (ResultCode)

| code              | description                               |Action                                      |
| ------------------|-------------------------------------------|--------------------------------------------|
|1                  |Error occured check your data              |                                            |
|100                |Auth error                                 |
|130,151            |Field contain invalid data                 |
|215                |American Express card Cvv2 must have 4 digits|
|216                |Wrong cvv2                                 |
|50x                |Communication error                        |Try again
|981                |Invalid Card number/Exp Month/Exp Year     |
|1034               |Terminal does not support given card type  |
|1040, 1041         |Error validating IP address.               |
|1045               |Duplicate transaction references           |Try again
|1072               |Pack is still closing                      |
|1802               |Wrong ammount value                        |
|7001               |Anti fraud                                 |


-----

## WebComponent

### Error codes


> Network Relayed errors

| code  | description   |
| ------|---------------|
|200    |Success        |

> Service errors

| code  | description   |
| ------|---------------|
|200    |Success        |


## Auth0

auth0.js sdk -> https://auth0.com/docs/libraries/auth0js/v9

SPA -> Implicit grant auth

invoke /authorize endpoint with prompt=none


## Gateways Api Authorization

> In order to use gateways serverless api's from web based hosted app:

You __*server side__ application must make a call to https://{{auth0_domain}}/oauth/token in order to get access_token with this body:

```
{
	"client_id":"{AUTH0_CLIENT_ID}",
	"client_secret":"{AUTH0_CLIENT_SECRET}",
	"audience":"{API_IDENTIFIER}",
	"grant_type":"client_credentials"
}
```

* Do not expose client_id, client_secret on browser
