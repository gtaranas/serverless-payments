import AWS from "aws-sdk";
require('dotenv').config()

AWS.config.update({
    region: "us-east-1",
    accessKeyId: "accessKey",
    secretAccessKey: "secretKey"
});
export function call(action, params) {
    const dynamoDb = new AWS.DynamoDB.DocumentClient({
        region: 'localhost',
        endpoint: 'http://localhost:8000',
        convertEmptyValues: true
      });
    return dynamoDb[action](params).promise();
}