export function success(event, body) {
    return buildResponse(200, event, body);
}
export function failure(event, body) {
    return buildResponse(500, event, body);
}

export function notFound(event, body) {
    return buildResponse(404, event, body);
}

function buildResponse(statusCode, event, body) {
    
    const ALLOWED_ORIGINS = [
        'http://payment.ime.com',
        'http://localhost:8080',
        'file://'
    ];

    
    let headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
    }
    
    if (event.headers) {
        const origin = event.headers.Origin;
        
        // if (ALLOWED_ORIGINS.includes(origin)) {
            headers = {
                'Access-Control-Allow-Origin': origin,
                'Access-Control-Allow-Credentials': true
            }
        // }  else {
        //     statusCode = 401
        // }

    }

    return {
        statusCode: statusCode,
        headers: headers,
        body: JSON.stringify(body) || null
    };
}