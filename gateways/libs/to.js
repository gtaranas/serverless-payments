export default async function to(promise) {
    let error = null, data = null
    
    data = await promise.catch(err => {error = err});
    console.log(error)

    return [error, data]
 }