export default class SoapRequest {    
    constructor (postItem){
        const args = {
            TransactionRequest: {
                Header: {
                    RequestType: 'FOLLOW_UP',
                    RequestMethod: 'SYNCHRONOUS',
                    MerchantInfo : {
                        AcquirerID: 'GR0' + postItem.AcquirerID,
                        MerchantID: postItem.MerchantID,
                        PosID: postItem.PosID,
                        ChannelType: '3DSecure',
                        User: postItem.User,
                        Password: postItem.Password,
                    }
                }, 
                Body: {
                    TransactionInfo: {
                        MerchantReference: postItem.MerchantReference
                    }
                }
            }
        }
     
        return args
    }
}