export default class SoapRequest {    
    constructor (postItem){
        const args = {
            Request: {
                AcquirerId: postItem.AcquirerId,
                MerchantId: postItem.MerchantId,
                PosId: postItem.PosId,
                Username: postItem.Username,
                Password: postItem.Password,
                RequestType:postItem.RequestType,
                CurrencyCode: postItem.CurrencyCode,
                MerchantReference: postItem.MerchantReference,
                Amount: postItem.Ammount,
                Installments: postItem.Installments,
                ExpirePreauth: postItem.ExpirePreauth,
                Bnpl: postItem.Bnpl,
                Parameters: postItem.params
            }
        }
     
        return args
    }
}