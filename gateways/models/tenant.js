import * as dynamoDbLib from "../libs/dynamodb-lib";
import uuid from "uuid"

export default class Tenant {
    
    constructor(id) {
        this.createdAt = Date.now()
        this.updatedAt = Date.now()
        this.tenant_id = id || uuid.v1()
        
        /// TODO: Add this on tenant profile creation for pass
        // var data = "LS976385";
        // var crypto = require('crypto');
        // var pass = crypto.createHash('md5').update(data).digest("hex")
    }

    async getByAudience(audience) {
        var tenant
        let params = {
            TableName: process.env.DYNAMODB_TABLE,
            FilterExpression: "audience = :audience",
            ExpressionAttributeValues: {
                ":audience": audience
            }
        }
        try {
            tenant = await dynamoDbLib.call("scan", params);
            console.log('tenant', tenant)
            this.tenant_id = tenant.Items[0].tenant_id
        } catch (e) {
            this.tenant_id = uuid.v1()
            console.log('failed to get tenant audience: ', e)
        }

        return tenant.Items[0] || null
    }

    async get () {
        var tenant = null
        const params = {
            TableName: process.env.DYNAMODB_TABLE,
            Key: {
                tenant_id: this.tenant_id
            }
        }
        try {
            tenant = await dynamoDbLib.call("get", params);
        }catch (e) {
            console.log('get tenant from db failed:', e)
        }

        return tenant.Item || null
    }

    save (tenant_info) {
        
        this.createdAt = new Date()
        this.updatedAt = new Date()

        let params = {
            TableName: process.env.DYNAMODB_TABLE,
            Item: Object.assign({ ...tenant_info, tenant_id: uuid.v1(), createdAt: this.createdAt, updatedAt: this.updatedAt })
        }
        
        return dynamoDbLib.call("put", params);
    }

    list () {
        let params = {
            TableName: process.env.DYNAMODB_TABLE
        }

        return dynamoDbLib.call("scan", params);
        
    }
}