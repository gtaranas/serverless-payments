import * as dynamoDbLib from "../libs/dynamodb-lib";
import { v1 } from "uuid"

export default class Transaction {
    
    constructor (type, tenant) {
        this.tenant = tenant
        this.type = type
    }


    async save (transaction) {
        let params = {
            TableName: process.env.DYNAMODB_TRANSACTIONS_TABLE,
        }

        console.log('update transaction -> ', transaction)
        if (transaction.transaction_id)
        {
            
            params.Item = { ...transaction, type: this.type || transaction.type, updatedAt: Date.now() }
        } else {
            console.log('new transaction tenant ->', this.tenant)
            params.Item = { ...transaction, transaction_id: v1(), type: this.type, tenant_id: this.tenant.tenant_id, createdAt: Date.now(), updatedAt: Date.now() }
        }

        await dynamoDbLib.call("put", params);

        return params.Item.transaction_id
    
    }
    
    async getActiveByRef (mref) {
        
        let transactions
        
        let params = {
            TableName: process.env.DYNAMODB_TRANSACTIONS_TABLE,
            FilterExpression: "MerchantReference = :mref and #mactive = :mactive",
            ExpressionAttributeNames:{
                "#mactive": "active"
            },
            ExpressionAttributeValues: {
                ":mref": mref,
                ":mactive": true
            }
        }
        try {
            transactions = await dynamoDbLib.call("scan", params);
            console.log(transactions)
        } catch (e) {
            console.log('failed to get active transaction: ', e)
        }

        return (transactions.Items && transactions.Items.length > 0) ? transactions.Items[0] : null
    }

    async get (transaction_id) {
        
        let transactions
        
        let params = {
            TableName: process.env.DYNAMODB_TRANSACTIONS_TABLE,
            Key: {
                transaction_id: transaction_id
            }
        }
        try {
            transactions = await dynamoDbLib.call("get", params);
            console.log(transactions)
        } catch (e) {
            console.log('failed to get transaction: ', e)
        }

        return (transactions.Item) ? transactions.Item : null
    }

    async getActive (transaction_id) {
        var transaction = null
        let params = {
            TableName: process.env.DYNAMODB_TRANSACTIONS_TABLE,
            FilterExpression: 'transaction_id = :hkey and #mactive = :mactive',
            ExpressionAttributeNames:{
                "#mactive": "active"
            },
            ExpressionAttributeValues: {
                ':hkey': transaction_id,
                ":mactive": true
            }
            
        }
        try {
            transaction = await dynamoDbLib.call("scan", params);
        }catch (e){
            console.log('get transaction from db failed:', e)
        }
        return (transaction.Items && transaction.Items.length > 0) ? transaction.Items[0] : null
    }

    list () {
        let params = {
            TableName: process.env.DYNAMODB_TRANSACTIONS_TABLE
        }

        return dynamoDbLib.call('scan', params);
        
    }
}