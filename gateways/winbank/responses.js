import { failure } from "../libs/response-lib";
import Tenant from "../models/tenant";
import Transaction from "../models/transaction";
import crypto from 'crypto';

function QueryStringToJSON(params) {
    var pairs = params.split('&')
    var result = {}
    pairs.forEach(pair => {
        pair = pair.split('=')
        result[pair[0]] = decodeURIComponent(pair[1] || '')
    })

    return JSON.parse(JSON.stringify(result))
}


async function validateBankResponse(bankResponse, transaction) {

    let valid = false

    if (!bankResponse || !transaction)
        valid = false

    try {
        let tenant = await new Tenant(transaction.tenant_id).get()

        let hashFields = transaction.TranTicket + tenant.PosId + tenant.AcquirerId + transaction.MerchantReference +
            bankResponse.ApprovalCode + bankResponse.Parameters + bankResponse.ResponseCode + bankResponse.SupportReferenceID +
            bankResponse.AuthStatus + bankResponse.PackageNo + bankResponse.StatusFlag

        let hashKey = crypto.createHash('sha256').update(hashFields).digest('hex').toString().toLocaleUpperCase()
        console.log(bankResponse.HashKey, hashKey)
        valid = bankResponse.HashKey == hashKey

    } catch (e) {
        console.log('failed to validate hashKey: ', e)
        valid = false
    }

    return valid
}

/**
 * Winbank response reaches this action
 * @param {*} event 
 * @param {*} context 
 * @param {*} callback 
 */
export const get = async (event, context, callback) => {

    const html = `
        <html>
            <style>
                h1 { color: #73757d; }
            </style>
            <body>
                <h1>Please wait..</h1>
                <script>
                    window.close()
                </script>
            </body>
        </html>`;

    const response = {
        statusCode: 200,
        headers: {
            'Content-Type': 'text/html',
        },
        body: html,
    };

    try {
        const DBTransaction = new Transaction('Callback')

        /**
         * 
         * User canceled the transaction
         * 
         */
        console.log('query params ->', event.queryStringParameters)
        console.log('body ->', event.body)
        if (event.queryStringParameters && event.queryStringParameters.MerchantReference) {
            try {
                let transaction = await DBTransaction.getActiveByRef(event.queryStringParameters.MerchantReference);
                if (transaction && transaction !== null)
                    await DBTransaction.save({ 
                        ...transaction,
                        active: false,
                        canceled: true,
                        type: 'callback'
                    })
                
            } catch (ex) {
                console.log('failed to cancel the transaction on db', ex)
            }

        } else {

            let bankResponse = QueryStringToJSON(event.body)
            
            // console.log('response', response)

            /**
             * Fail with Merchantreference has already been used in a successful transaction
             * 
             * event.body ->
             * 
             * "StatusFlag=Failure&MerchantReference=10004&HashKey=&Parameters=&ResultCode=1048&TransactionId=&SupportReferenceID=170240761
             * &ApprovalCode=&ResponseCode=&PackageNo=&AuthStatus=03&TransactionDateTime=-&ResponseDescription=&RetrievalRef=
             * &ResultDescription=Merchantreference+has+already+been+used+in+a+successful+transaction&CardType=1
             * &LanguageCode=el-GR&PaymentMethod=Card&ButtonSubmit=Submit"
             */

            console.log('bankResponse -> ', bankResponse)
            var params
            if (bankResponse.Parameters) {
                console.log(QueryStringToJSON(bankResponse.Parameters))
                params = QueryStringToJSON(bankResponse.Parameters)
            }

            if (event.queryStringParameters && event.queryStringParameters.result == 'fail') {
                try {

                    let transaction = (params && params.transactionId && params.transactionId != "") 
                    ? await DBTransaction.get(params.transactionId)
                    : await DBTransaction.getActive(bankResponse.MerchantReference)

                    if (transaction && transaction !== null)
                        await DBTransaction.save({ 
                            ...transaction,
                            ...bankResponse,
                            active: false,
                            type: 'callback'
                        })

                } catch (ex) {
                    console.log('failed to write response on transactions table', ex)

                }
            } else if (bankResponse && bankResponse.ResultCode) {
                if (bankResponse.ResultCode == "0") {
                    if (bankResponse.StatusFlag == 'Success') {
                        if (bankResponse.ResponseCode !== '11') {

                            // Success
                            let transaction = (params && params.transactionId && params.transactionId != "") 
                            ? await DBTransaction.get(params.transactionId)
                            : await DBTransaction.getActive(bankResponse.MerchantReference)

                            if (transaction && transaction !== null)
                                await DBTransaction.save({ 
                                    ...transaction,
                                    ...bankResponse,
                                    success: await validateBankResponse(bankResponse, transaction),
                                    active: false,
                                    type: 'callback'
                                })
                            

                        }
                    }
                }
            }

        }


        // callback is sending HTML back
        callback(null, response);

        // if (event.queryStringParameters.result == 'fail')
        //     callback(null, failure(JSON.parse('{"' + decodeURI(event.body).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')))
        // else
        //     callback(null, success(event.body))

    } catch (e) {
        console.log(e)
        callback(null, response)
    }
};


export const iframe = async (event, context, callback) => {
    try {
        // let dynamicHtml = '<p>Hey Unknown!</p>';
        // // check for GET params and use if available
        // if (event.queryStringParameters && event.queryStringParameters.name) {
        //     dynamicHtml = `<p>Hey ${event.queryStringParameters.name}!</p>`;
        // }


        const tenant = await new Tenant(event.pathParameters.tenant_id).get()

        if (tenant) {

            const DBTransaction = new Transaction()
            let transaction = await DBTransaction.get(event.pathParameters.transaction_id)
            if (transaction !== null) {

                // Create my compount per tenant ref id 
                var merchantRef = transaction.MerchantReference

                console.log('tenant', tenant)
                console.log('merchantRef', merchantRef)

                const html = `
                    <html>
                        <style>
                        h1 { color: #73757d; }
                        </style>
                        <body>
                        <h1>Please wait..</h1>
                        <form id="paymentForm" action="https://paycenter.piraeusbank.gr/redirection/pay.aspx" method="post">
                            <input name="AcquirerId" type="hidden" value="${tenant.AcquirerId}" /> 
                            <input name="MerchantId" type="hidden" value="${tenant.MerchantId}" /> 
                            <input name="PosId" type="hidden" value="${tenant.PosId}" /> 
                            <input name="User" type="hidden"  value="${tenant.Username}" /> 
                            <input name="MerchantReference" type="hidden" value="${merchantRef}" /> 
                            <input name="LanguageCode" type="hidden" value="el-GR" /> 
                            <input name="ParamBackLink" type="hidden" value="MerchantReference=${merchantRef}" />
                        </form>
                        <script>
                        
                        document.getElementById("paymentForm").submit();
                        
                        </script>
                        </body>
                    </html>`;

                const response = {
                    statusCode: 200,
                    headers: {
                        'Content-Type': 'text/html',
                    },
                    body: html,
                };

                // callback is sending HTML back
                callback(null, response);
            } else {
                callback(null, failure({
                    error: 'Transaction not found!'
                }))
            }
        }


    } catch (e) {
        console.log(e)
        callback(null, failure({
            error: e
        }))
    }
};