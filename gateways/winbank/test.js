'use strict';

require('./common');

const soap = require('soap')
// const EasySoap = require('easysoap');

var data = "LS976385";
var crypto = require('crypto');
var pass = crypto.createHash('md5').update(data).digest("hex")

var url = 'https://paycenter.piraeusbank.gr/services/tickets/issuer.asmx?wsdl';
var args = {
    Request: {
        AcquirerId: 14,
        MerchantId: 2133499495,
        PosId: 2131019924,
        Username: 'KT235056',
        Password: pass,
        RequestType: '02',
        CurrencyCode: 978,
        MerchantReference: 10001,
        Amount: 10,
        Installments: 0,
        ExpirePreauth: 0,
        Bnpl: 0,
        Parameters: ''
    }
}

// define soap params
// const params = {
//     host: 'https://paycenter.piraeusbank.gr',
//     path: '/services/tickets/issuer.asmx',
//     wsdl: '/services/tickets/issuer.asmx?WSDL',

//     // set soap headers (optional)
//     // headers: [{
//     //     'name'      : 'SOAPAction',
//     //      'value'    : 'item_value',
//     //      'namespace': 'item_namespace'
//     // }]
// }

// /*
//  * create the client
//  */
// var soapClient = EasySoap(params);

// /*
//  * get all available functions
//  */
// soapClient.getAllFunctions()
//     .then((functionArray) => {
//         console.log(functionArray);
//     })
//     .catch((err) => {
//         throw new Error(err);
//     });

// /*
//  * call soap method
//  */
// soapClient.call({
//         method: 'IssueNewTicket',
//         // attributes: {
//         //     'xmlns:envelop': 'http://schemas.xmlsoap.org/soap/envelope'
//         // },
//         params: args
//     })
//     .then((callResponse) => {
//         // console.log('r', callResponse)
//         console.log(callResponse.data); // response data as json
//         console.log(callResponse.body); // response body
//         console.log(callResponse.header); //response header
//     })
//     .catch((err) => {
//         throw new Error(err);
//     });

// options 1
// "overrideRootElement": {
//     "namespace": "xmlns:tns",
//     "xmlnsAttributes": [{
//       "name": "xmlns:xsi",
//       "value": "http://www.w3.org/2001/XMLSchema-instance"
//     }, {
//       "name": "xmlns:xsd",
//       "value": "http://www.w3.org/2001/XMLSchema"
//     },
//     {
//       "name": "xmlns:soap",
//       "value": "http://schemas.xmlsoap.org/soap/envelope/"
//     }]
//   }


soap.createClient(url, {
    forceSoap12Headers: false
}, function (err, client) {

    // client.setSOAPAction('http://piraeusbank.gr/paycenter/redirection/IssueNewTicket')

    if (err) {
        console.log('failed to create soap client', err);
        return
    }

    client['IssueNewTicket'](args, function (err, result) {
        if (err) {
            console.log('failed to call soap method', err);
            return
        }
        // console.log(result);
        // create a response
        console.log('result', result);
    });
});