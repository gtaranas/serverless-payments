import { success, failure } from "../libs/response-lib";
import Transaction from "../models/transaction";
import Tenant from "../models/tenant";
import SoapRequest from "../models/soapRequest";
import soap from 'soap'
import request from 'request';

const payment_url = 'https://paycenter.piraeusbank.gr/redirection/pay.aspx'
const token_url = 'https://paycenter.piraeusbank.gr/services/tickets/issuer.asmx?wsdl'

export const issue = async (event, context, callback) => {
    
    
    const payload = JSON.parse(event.body);
    console.log('event --->', event.requestContext.authorizer.claims.aud)
    console.log('context --->', context)

    try {
        const tenant = await new Tenant().getByAudience(event.requestContext.authorizer.claims.aud)
        
        if (tenant) {
            
            // Create my compount per tenant ref id 
            var merchantRef = `${tenant.tenant_id}_${payload.refId}`
            const DBTransaction = new Transaction('token', tenant)

            // Allways check if there is allready an open transaction with active ticket
            let transaction = await DBTransaction.getActiveByRef(merchantRef)

            let transaction_id = await DBTransaction.save({...transaction, active: true})

            console.log('trnid ->', transaction_id)

            let soapRequest = new SoapRequest({ 
                ...tenant,
                MerchantReference: merchantRef,
                Ammount: payload.ammount,
                params: `transactionId=${transaction_id}`
            })

            let ticket = await sendSoapRequest(soapRequest)
            
            transaction = await DBTransaction.get(transaction_id)

            await DBTransaction.save({ ...transaction, ...ticket, MerchantReference: merchantRef, Ammount: payload.ammount, active: true})

            
            callback(null, success(event, { payload: { MerchantReference: merchantRef, transaction_id: transaction_id, tenant_id: tenant.tenant_id }}))
            
            
        } else {
            callback(null, failure({
                status: false,
                error: "Tenant info not found."
            }));
        }
    } catch (e) {
        
        console.log('error on ticket transaction:', e)

        callback(null, failure({
            status: false,
            error: e.message
        }));
    }
    
};



const submitPayment = (formData) => new Promise((resolve, reject) => 
    request.post({
        url: payment_url,
        formData: formData
    }, function optionalCallback(err, httpResponse, body) {
        if (err) {
            console.log(err)
            reject(err)
        }

        resolve(body)
    })
)

const sendSoapRequest = (soapRequest) => new Promise((resolve, reject) => 
    soap.createClient(token_url, {forceSoap12Headers: true}, function (err, client) {
        if (err){
            console.log('failed to create soap client', err); 
            return reject(err);
        }

        client['IssueNewTicket'](soapRequest, function (err, result) {
            if (err){
                console.log('failed to call soap method', err); 
                return reject(err);
            }
            
            resolve(result.IssueNewTicketResult)
            
        });
    })
);