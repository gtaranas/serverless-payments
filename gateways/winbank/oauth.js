import request from 'request'

export const token = (event, context, callback) => {
    const payload = JSON.parse(event.body);
    const options = { 
        method: 'POST',
        url: 'https://gtaranas.eu.auth0.com/oauth/token',
        json: true,
        body: 
        {
            client_id: payload.client_id,
            client_secret: payload.client_secret,
            audience: payload.audience,
            grant_type: "client_credentials"
        }
    };
    
    request(options, function (error, response, body) {
        if (error)
            return callback(null, callback({
                statusCode: 500,
                error: error
            }));
            
        callback(null, {
            statusCode : response.statusCode,
            body
        })
    });

}