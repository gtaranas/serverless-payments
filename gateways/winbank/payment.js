import * as dynamoDbLib from "../libs/dynamodb-lib";
import { success, failure } from "../libs/response-lib";
import request from 'request';

const payment_url = 'https://paycenter.piraeusbank.gr/redirection/pay.aspx'

export const submit = async (event, context, callback) => {
    try {
        let resp = await submitPayment(event)
        callback(null, success(resp))
    } catch(e) {
        console.log(e)
        callback(null, failure({error: e}))
    }    
};

const submitPayment = (formData) => new Promise((resolve, reject) => {
    let data = "LS976385";
    let crypto = require('crypto');
    let pass = crypto.createHash('md5').update(data).digest("hex")

    // resolve({
    //     AcquirerId: 14,
    //     MerchantId: 2133499495,
    //     PosId: 2131019924,
    //     User: 'KT235056',
    //     Password: pass,
    //     RequestType: '02',
    //     CurrencyCode: 978,
    //     MerchantReference: 10001,
    //     Amount: 10,
    //     Installments: 0,
    //     ExpirePreauth: 0,
    //     Bnpl: 0,
    //     Parameters: ''
    // })

    const formData = {
        AcquirerId: 14,
        MerchantId: 2133499495,
        PosId: 2131019924,
        User: 'KT235056',
        MerchantReference: 10001,
        Amount: 10,
        LanguageCode: 'el-GR',
        ParamBackLink: ''
    }
   

    request.post({
        url: payment_url,
        formData: formData
    }, function optionalCallback(err, httpResponse, body) {
        if (err) {
            console.log(err)
            reject(err)
        }

        resolve(body)
    })
}
);