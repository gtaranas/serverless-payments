import { success, failure, notFound } from "../libs/response-lib";
import Tenant from "../models/tenant";
import Transaction from "../models/transaction"
import SoapRequest from "../models/soapFollowUpRequest";
import soap from 'soap'
import { call } from "../libs/dynamodb-lib";

const followup_url = 'https://paycenter.winbank.gr/services/paymentgateway.asmx?wsdl'

export const list = async (event, context, callback) => {
    try {
        console.log('tenant_id -->', event.pathParameters.tenant_id)
        const tenant = await new Tenant(event.pathParameters.tenant_id).get()
        const transcations = await new Transaction("", tenant).list()

        callback(null, success(transcations.Items))
    } catch (e){
        console.log('failed to read tenant info --> ', e)
        callback(null, failure({
            status: false,
            error: "Tenant info not found."
        }));
    }
    
}


export const get = async (event, context, callback) => {
    try {
        const DBTransaction = new Transaction()
        let transaction = await DBTransaction.get(event.pathParameters.transaction_id)
        if (transaction !== null)
            callback(null, success(event, {
                resultCode: transaction.ResultCode,
                responseCode: transaction.ResponseCode,
                statusFlag: transaction.StatusFlag,
                followUpResultCode: transaction.FollowUpResultCode,
                followUpResponseCode: transaction.FollowUpResponseCode,
                followUpStatusFlag: transaction.FollowUpStatusFlag,
                active: transaction.active,
                canceled: transaction.canceled,
                type: transaction.type
            }))
        else
            callback(null, notFound({
                status: false,
                error: "No transaction found."
            }))
        
    } catch (e){
        
        console.log('failed to read transaction info --> ', e)

        callback(null, failure({
            status: false,
            error: "No transaction found."
        }));
    }
}

export const getActive = async (event, context, callback) => {
    try {
        // console.log('tenant_id -->', event.pathParameters.tenant_id)
        // const tenant = await new Tenant(event.pathParameters.tenant_id).get()
        const DBTransaction = new Transaction()
        let transaction = await DBTransaction.get(event.pathParameters.transaction_id)
        if (transaction !== null) {
            
            let tenant = await new Tenant(transaction.tenant_id).get();
            
            if (transaction.type != 'Callback') // Dont overwrite transaction from followUp response if already have callback response
                await followUpRequest(tenant, transaction)

            transaction = await DBTransaction.get(event.pathParameters.transaction_id)
            
            if (transaction && transaction !== null)
                callback(null, success(event, {
                    resultCode: transaction.ResultCode,
                    responseCode: transaction.ResponseCode,
                    statusFlag: transaction.StatusFlag,
                    followUpResultCode: transaction.FollowUpResultCode,
                    followUpResponseCode: transaction.FollowUpResponseCode,
                    followUpStatusFlag: transaction.FollowUpStatusFlag,
                    active: transaction.active,
                    canceled: transaction.canceled,
                    type: transaction.type
                }))
            else
                callback(null, notFound({
                    status: false,
                    error: "No Active transaction found."
                }))
        }
        else
            callback(null, notFound({
                status: false,
                error: "No Active transaction found."
            }))
    } catch (e){
        
        console.log('failed to read transaction info --> ', e)

        callback(null, failure({
            status: false,
            error: "Tenant info not found."
        }));
    }
    
}

export const cancel = async (event, context, callback) => {
    try {
        // console.log('tenant_id -->', event.pathParameters.tenant_id)
        // const tenant = await new Tenant(event.pathParameters.tenant_id).get()
        const DBTransaction = new Transaction()
        let transaction = await new DBTransaction.get(event.pathParameters.transaction_id)

        if (transaction !== null){
            
            await DBTransaction.save({ 
                ...transaction,
                active: false,
                canceled: true,
                type: 'component'
            })

            callback(null, success({transaction_id: transaction.transaction_id, active: transaction.active}))

        } else {
            callback(null, notFound({
                status: false,
                error: "Transaction info not found."
            }))
        }
    } catch (e){
        console.log('failed to cancel transaction --> ', e)
        callback(null, failure({
            status: false,
            error: "Transaction cancel error."
        }));
    }
    
}

export const followUp = async (event, context, callback) => {
    
    try {
        
        let tenant = await new Tenant(transactions.Items[0].tenant_id).get()

        let transaction = await new Transaction().get(event.pathParameters.transaction_id)
        if (transaction !== null) {
            await followUpRequest(tenant)
        }

        callback(null, success({transaction_id: transaction.transaction_id, active: transaction.active, success: transaction.success}))
    } catch(e) {
        console.log('failed to execute follow up --> ', e)
        callback(null, failure({
            status: false,
            error: "Follow up failed."
        }));
    }
}

export const followUpRequest = async (tenant, transaction) => {
    
    const DBTransaction = new Transaction('followUp', tenant)

    let soapRequest = new SoapRequest({
        AcquirerID: tenant.AcquirerId,
        MerchantID: tenant.MerchantId,
        PosID: tenant.PosId,
        User: tenant.Username,
        Password: tenant.Password,      
        MerchantReference: transaction.MerchantReference
    })
    
    let followupResponse = await sendSoapRequest(soapRequest)
    if (followupResponse) {
        
        console.log('response header -> ', followupResponse.TransactionResponse.Header)
        console.log('response body -> ', followupResponse.TransactionResponse.Body)

        let resultCode = followupResponse.TransactionResponse.Header.ResultCode
        let resultDescription = followupResponse.TransactionResponse.Header.ResultDescription
        let supportReferenceID = followupResponse.TransactionResponse.Header.SupportReferenceID
        let statusFlag = followupResponse.TransactionResponse.Body.TransactionInfo.StatusFlag
        let responseCode = followupResponse.TransactionResponse.Body.TransactionInfo.ResponseCode
        let responseDescription = followupResponse.TransactionResponse.Body.TransactionInfo.ResponseDescription

        // If result code == 1010 (Wrong original transaction) means there is no transaction stored on paycenter yet
        if (resultCode !== 1010) 
            await DBTransaction.save({ 
                ...transaction,
                StatusFlag: statusFlag,
                ResultCode: resultCode,
                ResultDescription: resultDescription,
                ResponseCode: responseCode,
                ResponseDescription: responseDescription,
                SupportReferenceID: supportReferenceID,
                type: 'followup',
                active: false
            })
    }
}

const sendSoapRequest = (soapRequest) => new Promise((resolve, reject) => 
    soap.createClient(followup_url, {forceSoap12Headers: false}, function (err, client) {
        if (err){
            console.log('failed to create soap client', err); 
            return reject(err);
        }
        
        client['ProcessTransaction'](soapRequest, function (err, result, rawResponse, soapHeader, rawRequest) {
            // console.log(rawRequest)
            if (err){
                console.log('failed to call soap method', err); 
                return reject(err);
            }
            
            resolve(result)
            
        });
    })
);